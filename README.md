This repository contains a few examples of creating beautiful vector graphic diagrams using  
metapost (https://www.tug.org/metapost.html).  

Metapost is installed automatically when latex is installed in linux/Mac.  
The above link contains various tutorials/manuals for more creating more complex pictures.  
The following is my personal recommendation to start with  
= Learning metapost by doing, authored by Andre Heck (2005), which can be downloaded from https://staff.fnwi.uva.nl/a.j.p.heck/Courses/mptut.pdf  

Explanation of files
---------------------
Each folder contains the following files, assuming the filename to be test  
(1) test.mp -- this is the metapost source file. All edits should be made here. This file produces test1.mps.  
(2) test.tex -- compiling this latex source file produces 2dcut.pdf  
There can be more than one pdf output depending on the metapost source file: see example2.  


Compiling
---------
Examples presented in each folder can be compiled by typing "sh test.sh" in the terminal  

Debugging
---------
In case, if latex produces some error, copy two files (supp-mis.tex and supp-pdf.tex) given in example 1 folder into the working directory.  

